const { EmbedBuilder } = require('discord.js');
const { SlashCommandBuilder } = require('discord.js');
const p = require('phin');

module.exports = {
    cooldown: 10,
    data: new SlashCommandBuilder()
        .setName('vn')
        .setDescription('Searches info on the given VN.')
        .addStringOption(option =>
			option.setName('name')
				.setDescription('Name of the VN')
				.setRequired(true)),
    async execute(interaction) {
        // get the name of the VN
        const vn_name = interaction.options.getString('name', true);

        try {
            // POST to vndb API
            const res = await p({
                url: 'https://api.vndb.org/kana/vn',
                parse: 'json',
                method: 'POST',
                data: {
                    'filters': ['search', '=', `${vn_name}`],
                    'fields': 'title, image.url, released, length_minutes, rating, votecount, developers.name',
                    'sort': 'searchrank',
                    'results': 1,
                },
            });
            const data = res.body.results[0];

            // Send embed with info
            const vndb_embed = new EmbedBuilder()
                .setColor(0x051523)
                .setTitle(`${data.title}`)
                .setURL(`https://vndb.org/${data.id}`)
                .setAuthor({ name: 'vndb', url: 'https://vndb.org/' })
                .addFields(
                    { name: 'Information', value:
                    `Developer: [${data.developers[0].name}](https://vndb.org/${data.developers[0].id})\n ` +
                    `Length: ${Math.round(parseInt(data.length_minutes) / 60)} hours\n` +
                    `Rating: ${data.rating} (${data.votecount} votes)\n` +
                    `Released: ${data.released}`,
                    },
                )
                .setImage(`${data.image.url}`)
                .setTimestamp();
            await interaction.reply({ embeds: [vndb_embed] });

        } catch (error) {
            console.error(error);
            await interaction.reply(`There was an error while searching data on \`${vn_name}\`:\n\`${error.message}\``);
        }
    },
};