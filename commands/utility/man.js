const { EmbedBuilder } = require('discord.js');
const { SlashCommandBuilder } = require('discord.js');
const { mal_clientId } = require('../../config.json');
const axios = require('axios');


module.exports = {
    cooldown: 10,
    data: new SlashCommandBuilder()
        .setName('man')
        .setDescription('Searches info on the given manga.')
        .addStringOption(option =>
			option.setName('name')
				.setDescription('Name of the manga')
				.setRequired(true)),
    async execute(interaction) {
        // get the name
        const manga_name = interaction.options.getString('name', true);
        // search info with the name
        try {
            // define authentication header
            const mal_auth = {
                'X-MAL-CLIENT-ID': mal_clientId,
            };
            // GET to mal API for title Id
            const res_id = await axios({
                url: `https://api.myanimelist.net/v2/manga?q=${manga_name}&limit=1`,
                headers: mal_auth,
                method: 'GET',
            });

            // abort if mal finds no results
            if (res_id.data.data.length == 0) {
                await interaction.reply(`No results for "${manga_name}".`);
                return;
            }
            const title_id = res_id.data.data[0].node.id;

            // GET to mal API for info on Id
            const res_data = await axios({
                url: `https://api.myanimelist.net/v2/manga/${title_id}?fields=id,title,main_picture,start_date,end_date,mean,num_scoring_users,authors`,
                headers: mal_auth,
                method: 'GET',
            });
            const data = res_data.data;
            console.log(data.authors);

            const mal_embed = new EmbedBuilder()
                .setColor(0x051523)
                .setTitle(`${data.title}`)
                .setURL(`https://myanimelist.net/anime/${data.id}`)
                .setAuthor({ name: 'MAL', url: 'https://myanimelist.net/' })
                .addFields(
                    { name: 'Information', value:
                    `[Author](https://myanimelist.net/people/${data.authors[0].node.id})\n` +
                    `Rating: ${data.mean} (${data.num_scoring_users} votes)\n` +
                    `Released: ${data.start_date} - ${data.end_date}`,
                    },
                )
                .setImage(`${data.main_picture.large}`)
                .setTimestamp();
            await interaction.reply({ embeds: [mal_embed] });

        } catch (error) {
            console.error(error);
            await interaction.reply(`There was an error while searching data on \`${manga_name}\`:\n\`${error.message}\``);
        }
    },
};