const { EmbedBuilder } = require('discord.js');
const { SlashCommandBuilder } = require('discord.js');
const { XMLParser } = require('fast-xml-parser');
const closest_match = require('closest-match');
const { anidb_client, anidb_client_ver } = require('../../config.json');
const axios = require('axios');


module.exports = {
    cooldown: 10,
    data: new SlashCommandBuilder()
        .setName('ani')
        .setDescription('Searches info on the given anime.')
        .addStringOption(option =>
			option.setName('name')
				.setDescription('Name of the anime')
				.setRequired(true)),
    async execute(interaction) {
        // get the name
        const anime_name = interaction.options.getString('name', true);
        // search for the closest match
        const anime_title = closest_match.closestMatch(anime_name, interaction.client.anidb_titles);
        // get id
        const anime_id = interaction.client.anidb_ids[anime_title];
        // get info by id
        try {
            // GET to anidb API
            axios({
                url: `http://api.anidb.net:9001/httpapi?request=anime&client=${anidb_client}&clientver=${anidb_client_ver}&protover=1&aid=${anime_id}`,
                parse: 'string',
                method: 'GET',
                responseType: 'text',
            }).then(async function(res) {
                const options = {
                    ignoreAttributes: false,
                };
                const parser = new XMLParser(options);
                const data = parser.parse(res.data).anime;
                // console.log(data);
                // console.log(data.titles.title);
                // console.log(data.creators.name);
                const ani_embed = new EmbedBuilder()
                    .setColor(0x051523)
                    .setTitle(`${data.titles.title[0]['#text']}`)
                    .setURL(`https://anidb.net/anime/${anime_id}`)
                    .setAuthor({ name: 'anidb', url: 'https://anidb.net/' })
                    .addFields(
                        { name: 'Information', value:
                        `Episodes: ${data.episodecount}\n` +
                        `Rating: ${data.ratings.permanent['#text']} (${data.ratings.permanent['@_count']} votes)\n` +
                        `Aired: ${data.startdate} - ${data.enddate}`,
                        },
                    )
                    .setImage(`https://cdn-eu.anidb.net/images/main/${data.picture}`)
                    .setTimestamp();
                await interaction.reply({ embeds: [ani_embed] });
            });
        } catch (error) {
            console.error(error);
            await interaction.reply(`There was an error while searching data on \`${anime_name}\`:\n\`${error.message}\``);
        }
    },
};