const { EmbedBuilder } = require('discord.js');
const { SlashCommandBuilder } = require('discord.js');
const { mal_clientId } = require('../../config.json');
const axios = require('axios');


module.exports = {
    cooldown: 10,
    data: new SlashCommandBuilder()
        .setName('mal')
        .setDescription('Searches info on the given anime.')
        .addStringOption(option =>
			option.setName('name')
				.setDescription('Name of the anime')
				.setRequired(true)),
    async execute(interaction) {
        // get the name
        const anime_name = interaction.options.getString('name', true);
        // search info with the name
        try {
            // define authentication header
            const mal_auth = {
                'X-MAL-CLIENT-ID': mal_clientId,
            };
            // GET to mal API for title Id
            const res_id = await axios({
                url: `https://api.myanimelist.net/v2/anime?q=${anime_name}&limit=1`,
                headers: mal_auth,
                method: 'GET',
            });

            // abort if mal finds no results
            if (res_id.data.data.length == 0) {
                await interaction.reply(`No results for "${anime_name}".`);
                return;
            }
            const title_id = res_id.data.data[0].node.id;

            // GET to mal API for info on Id
            const res_data = await axios({
                url: `https://api.myanimelist.net/v2/anime/${title_id}?fields=id,title,main_picture,start_date,end_date,mean,num_scoring_users,num_episodes,studios`,
                headers: mal_auth,
                method: 'GET',
            });
            const data = res_data.data;

            const mal_embed = new EmbedBuilder()
                .setColor(0x051523)
                .setTitle(`${data.title}`)
                .setURL(`https://myanimelist.net/anime/${data.id}`)
                .setAuthor({ name: 'MAL', url: 'https://myanimelist.net/' })
                .addFields(
                    { name: 'Information', value:
                    `Studio: ${data.studios[0].name}\n` +
                    `Episodes: ${data.num_episodes}\n` +
                    `Rating: ${data.mean} (${data.num_scoring_users} votes)\n` +
                    `Aired: ${data.start_date} - ${data.end_date}`,
                    },
                )
                .setImage(`${data.main_picture.large}`)
                .setTimestamp();
            await interaction.reply({ embeds: [mal_embed] });

        } catch (error) {
            console.error(error);
            await interaction.reply(`There was an error while searching data on \`${anime_name}\`:\n\`${error.message}\``);
        }
    },
};