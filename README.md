# antenna-bot

antenna-bot is a Discord bot that can query information about titles in the following databases:

 - [vndb](https://vndb.org/) (/vn command)
 - [anidb](https://anidb.net/) (/ani)
 - [mal (anime)](https://myanimelist.net/) (/mal)
 - [mal (manga)](https://myanimelist.net/) (/man)

## Setting up the bot

For the bot to work it needs two files that are not provided by the repository.

### Config file

The user needs to provide a file named `config.json` with the following contents:
```
{
    "token": "discord-bot-token",
    "clientId": "discord-bot-clientId",
	"guildId": "discord-server-id",
    "anidb_client": "anidb-client-id",
    "anidb_client_ver": anidb-client-version,
    "mal_clientId": "mal-clientId"
}
```
To get the token and clientId, a Discord bot can be made [here](https://discord.com/developers/applications).

New anidb client can be made [here](https://anidb.net/software/add).

New MAL client can be made [here](https://myanimelist.net/apiconfig).

### Anidb titles

Anidb API does not provide a search feature so the searching is done by the bot. This requires the full list of titles in the database. The list can be downloaded from http://anidb.net/api/anime-titles.dat.gz. Unzip the `.gz` file and put the resulting `.dat` file in the `data` directory. 

