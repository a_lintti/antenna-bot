const { Events } = require('discord.js');


module.exports = {
	name: Events.ClientReady,
	once: true,
	execute(client) {
		console.log(`Ready! Logged in to Discord as ${client.user.tag}`);
	},
};
