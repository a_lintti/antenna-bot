const fs = require('node:fs');
const readline = require('readline');
const path = require('node:path');
// Require the necessary discord.js classes
const { Client, Collection, GatewayIntentBits } = require('discord.js');
const { token } = require('./config.json');

// Create a new client instance
const client = new Client({ intents: [GatewayIntentBits.Guilds] });
client.commands = new Collection();
client.cooldowns = new Collection();

// Retrieve command files
const foldersPath = path.join(__dirname, 'commands');
const commandFolders = fs.readdirSync(foldersPath);

for (const folder of commandFolders) {
	const commandsPath = path.join(foldersPath, folder);
	const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
	for (const file of commandFiles) {
		const filePath = path.join(commandsPath, file);
		const command = require(filePath);
		// Set a new item in the Collection with the key as the command name and the value as the exported module
		if ('data' in command && 'execute' in command) {
			client.commands.set(command.data.name, command);
		} else {
			console.log(`[WARNING] The command at ${filePath} is missing a required "data" or "execute" property.`);
		}
	}
}

// Retrieve event files
const eventsPath = path.join(__dirname, 'events');
const eventFiles = fs.readdirSync(eventsPath).filter(file => file.endsWith('.js'));

for (const file of eventFiles) {
	const filePath = path.join(eventsPath, file);
	const event = require(filePath);
	if (event.once) {
		client.once(event.name, (...args) => event.execute(...args));
	} else {
		client.on(event.name, (...args) => event.execute(...args));
	}
}

// Make the anidb title array and id map
const ani_fileStream = fs.createReadStream('data/anime-titles.dat');
const ani_rl = readline.createInterface({
	input: ani_fileStream,
	crlfDelay: Infinity,
});
client.anidb_titles = [];
client.anidb_ids = {};
async function populate_ani_titles() {
	const languages = ['en', 'fi', 'x-jat'];
	for await (const line of ani_rl) {
		if (Array.from(line.trim())[0] === '#') {
			continue;
		}
		const ls = line.split('|');
		if (languages.includes(ls[2])) {
			client.anidb_titles.push(ls[3]);
			client.anidb_ids[ls[3]] = ls[0];
		}
	}
}
populate_ani_titles();

// Log in to Discord with your client's token
client.login(token);
